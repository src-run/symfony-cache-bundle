<?php

/*
 * This file is part of the Teavee Object Caching Bundle.
 *
 * (c) Scribe Inc.     <oss@scr.be>
 * (c) Rob Frawley 2nd <rmf@scr.be>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Scribe\Teavee\ObjectCacheBundle\DependencyInjection;

use Scribe\WonkaBundle\Component\DependencyInjection\AbstractEnableableExtension;

/**
 * Class ScribeTeaveeObjectCacheExtension.
 */
class ScribeTeaveeObjectCacheExtension extends AbstractEnableableExtension
{
}

/* EOF */
